package com.example.android.tflitecamerademo;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;


public class acceuil extends AppCompatActivity {
    private Button btnPhoto;
    private Button btnGallery;
    private int RESULT_LOAD_IMAGE=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acceuil);

        btnPhoto = (Button) findViewById(R.id.btn1);

        btnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            //accès à la vue appareil photo
                Intent intent = new Intent(acceuil.this, CameraActivity.class);
                startActivity(intent);

            }
        });

        btnGallery =(Button) findViewById(R.id.btn2);
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
            }
        });








    }



}
